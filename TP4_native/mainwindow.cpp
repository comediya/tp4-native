﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QGraphicsScene>
#include <QRegularExpression>
#include <QPen>
#include <QBrush>
#include <QMessageBox>
#include <QTranslator>
#include <QActionGroup>
#include <QDir>

/**
 * @class MainWindow
 * @brief Fenêtre principale de l'application.
 */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->generer, &QPushButton::clicked, this, &MainWindow::genererCodeBarres);
    connect(ui->effacer, &QPushButton::clicked, this, &MainWindow::viderCodeBarre);

     m_langPath = ":/traduction/langue";
     changerLangue(QString("fr_ca"));
     creerMenuLangue();

}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief Cette fonction génère un code à barres à partir de l'entrée utilisateur.
 */
void MainWindow::genererCodeBarres() {
    QString entree = ui->lineEdit->text();

    // Valide l'entrée
    if (!entree.contains(QRegularExpression("^[A-Z0-9 \\-\\$\\%\\.\\/\\+]*$"))) {
        QMessageBox::warning(this, tr("Erreur"), tr("Caractères invalides dans l'entrée."));
        return;
    }

    // Ajoute les caractères de début et de fin
    entree.prepend('*');
    entree.append('*');

    // Dessine le code à barres
    QGraphicsScene *scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    int x = 0;
    for (QChar c : entree) {
        int index = CONVERSION.indexOf(c);
        for (int i = 0; i < 9; i++) {
            int largeur;
            QPen ecrire;
            if (code39[index][i]) {
                largeur = 2;
                ecrire.setColor(Qt::black);
            } else {
                largeur = 1;
                ecrire.setColor(Qt::white);
            }
            QBrush pinceau(ecrire.color());
            scene->addRect(x, 0, largeur, 50, ecrire, pinceau);
            x += largeur;
        }

        // Ajoute un espace après chaque caractère
        QPen styloBlanc(Qt::white);
        QBrush pinceauBlanc(Qt::white);
        scene->addRect(x, 0, 1, 50, styloBlanc, pinceauBlanc);
        x += 1;
    }
}



/**
 * @brief Efface le contenu de la ligne de saisie de texte et le code barre.
 */
void MainWindow::viderCodeBarre() {
    ui->lineEdit->clear();

    ui->graphicsView->scene()->clear();
}


/**
 * @brief Crée le menu de langue.
 */
void MainWindow::creerMenuLangue() {

    QActionGroup *lGroupeLangue = new QActionGroup(ui->menuLangue);
    lGroupeLangue->setExclusive(true);

    connect(lGroupeLangue, &QActionGroup::triggered, this,
            &MainWindow::slot_menu_action_triggered);

    // Va chercher le langage par défaut sur cet ordi
    QString localeSysteme = QLocale::system().name();       // e.g. "fr_CA"
    localeSysteme.truncate(localeSysteme.lastIndexOf('_')); // e.g. "fr"

    // m_langPath = QApplication::applicationDirPath(); //Dans un environnement en production, il faudrait utiliser ce path

    QDir dir(m_langPath);
    QStringList listeNomDeFichiers = dir.entryList(QStringList("traduction_*.qm"));

    for (int i = 0; i < listeNomDeFichiers.size(); ++i) {
        // extraction du langage à partir du nom de fichier
        QString localeLangue;
        QString localeLangueEtPays;
        localeLangue = listeNomDeFichiers[i];                         // "traduction_en_us.qm"
        localeLangue.truncate(localeLangue.lastIndexOf('.'));      // "traduction_en_us"
        localeLangueEtPays = localeLangue;
        localeLangueEtPays.remove(0,localeLangueEtPays.indexOf('_') +1); // "en_us"
        localeLangue.truncate(localeLangue.lastIndexOf('_'));      // "traduction_en"
        localeLangue.remove(0, localeLangue.lastIndexOf('_') + 1); // "en"

        QString nomDeLangue = QLocale(localeLangue).nativeLanguageName(); // le nom de la langue dans cette langue
        nomDeLangue = nomDeLangue.left(1).toUpper() + nomDeLangue.mid(1); //première lettre en majuscule

        QIcon ico(QString("%1/%2.png").arg(m_langPath).arg(localeLangue)); //ne sert pas pour l'instant.
        QAction *action = new QAction(ico, nomDeLangue, this);

        action->setCheckable(true);
        action->setData(localeLangueEtPays);

        ui->menuLangue->addAction(action);
        lGroupeLangue->addAction(action);

        // Sélectionne la langue du système par défaut dans le menu
        if (localeSysteme == localeLangue) {
            action->setChecked(true);
        }
    }
}



/**
 * @brief Slot appelé lorsqu'une action du menu est déclenchée.
 * @param action L'action du menu déclenchée.
 */
void MainWindow::slot_menu_action_triggered(QAction *action)
{
    if (0 != action) {
        // Charge le langage selon le data de l'action
        changerLangue(action->data().toString());
    }
}


/**
 * @brief Change la langue de l'application.
 * @param aLangueEtPays Le code de langue et de pays.
 */
void MainWindow::changerLangue(QString aLangueEtPays) {
    if (m_langCourrant != aLangueEtPays) {
        QTranslator* lTraducteur = new QTranslator(this);
        qApp->removeTranslator(lTraducteur);
        QString lLangue = m_langPath + "/traduction_" + aLangueEtPays + ".qm";
        if(lTraducteur->load(lLangue)) {
            qApp->installTranslator(lTraducteur);
            ui->retranslateUi(this);
            m_langCourrant = aLangueEtPays;
        } else {
            qDebug() << "Fichier de langue non trouvé";
        }
    }
}

