<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translatorcomment>MainWindow</translatorcomment>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="27"/>
        <source>Generate</source>
        <translatorcomment>Generer</translatorcomment>
        <translation>Generate</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="50"/>
        <source>Effacer</source>
        <translation>Erase</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="67"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="83"/>
        <source>Erreur</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="83"/>
        <source>Caractères invalides dans l&apos;entrée.</source>
        <translation>Invalid characters in input.</translation>
    </message>
</context>
</TS>
