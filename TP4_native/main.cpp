#include "mainwindow.h"

#include <QApplication>


/**
 * @brief Fonction principale de l'application.
 * @param argc Le nombre d'arguments de la ligne de commande.
 * @param argv Les arguments de la ligne de commande.
 * @return Le code de sortie de l'application.
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
