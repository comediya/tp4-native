#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE



/**
 * @class MainWindow
 * @brief Fenêtre principale de l'application.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Constructeur de MainWindow.
     * @param parent Le widget parent.
     */
    MainWindow(QWidget *parent = nullptr);

    /**
     * @brief Destructeur de MainWindow.
     */
    ~MainWindow();

private slots:

    /**
     * @brief Slot pour générer des codes-barres.
     */
    void genererCodeBarres();

    /**
     * @brief Slot pour vider les codes-barres.
     */
    void viderCodeBarre();

    /**
     * @brief Slot appelé lorsqu'une action du menu est déclenchée.
     * @param action L'action du menu déclenchée.
     */
    void slot_menu_action_triggered(QAction *action);
private:
    Ui::MainWindow *ui; /**< Objet interface utilisateur. */
    QList<QLabel*> barcodeLabels; /**< Liste des étiquettes de codes-barres. */
    const QString CONVERSION = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 -$%./+"; /**< Table de conversion pour les codes-barres. */
    int code39[44][9] = {
        {1,0,0,0,0,1,0,0,1},   //A 0
        {0,0,1,0,0,1,0,0,1},   //B 1
        {1,0,1,0,0,1,0,0,0},   //C 2
        {0,0,0,0,1,1,0,0,1},   //D 3
        {1,0,0,0,1,1,0,0,0},   //E 4
        {0,0,1,0,1,1,0,0,0},   //F 5
        {0,0,0,0,0,1,1,0,1},   //G 6
        {1,0,0,0,0,1,1,0,0},   //H 7
        {0,0,1,0,0,1,1,0,0},   //I 8
        {0,0,0,0,1,1,1,0,0},   //J 9
        {1,0,0,0,0,0,0,1,1},   //K 10
        {0,0,1,0,0,0,0,1,1},   //L 11
        {1,0,1,0,0,0,0,1,0},   //M 12
        {0,0,0,0,1,0,0,1,1},   //N 13
        {1,0,0,0,1,0,0,1,0},   //O 14
        {0,0,1,0,1,0,0,1,0},   //P 15
        {0,0,0,0,0,0,1,1,1},   //Q 16
        {1,0,0,0,0,0,1,1,0},   //R 17
        {0,0,1,0,0,0,1,1,0},   //S 18
        {0,0,0,0,1,0,1,1,0},   //T 19
        {1,1,0,0,0,0,0,0,1},   //U 20
        {0,1,1,0,0,0,0,0,1},   //V 21
        {1,1,1,0,0,0,0,0,0},   //W 22
        {0,1,0,0,1,0,0,0,1},   //X 23
        {1,1,0,0,1,0,0,0,0},   //Y 24
        {0,1,1,0,1,0,0,0,0},   //Z 25
        {0,0,0,1,1,0,1,0,0},   //0 26
        {1,0,0,1,0,0,0,0,1},   //1 27
        {0,0,1,1,0,0,0,0,1},   //2 28
        {1,0,1,1,0,0,0,0,0},   //3 29
        {0,0,0,1,1,0,0,0,1},   //4 30
        {1,0,0,1,1,0,0,0,0},   //5 31
        {0,0,1,1,1,0,0,0,0},   //6 32
        {0,0,0,1,0,0,1,0,1},   //7 33
        {1,0,0,1,0,0,1,0,0},   //8 34
        {0,0,1,1,0,0,1,0,0},   //9 35
        {0,1,1,0,0,0,1,0,0},   //espace 36
        {0,1,0,0,0,0,1,0,1},   //-     37
        {0,1,0,1,0,1,0,0,0},   //$ 38
        {0,0,0,1,0,1,0,1,0},   //% 39
        {1,1,0,0,0,0,1,0,0},   //. 40
        {0,1,0,1,0,0,0,1,0},   // / 41
        {0,1,0,0,0,1,0,1,0},   //+ 42
        {0,1,0,0,1,0,1,0,0}   //* 43
    }; /**< Tableau pour la génération des codes-barres. */

    /**
     * @brief Change la langue de l'application.
     * @param nomLangue Le nom de la langue.
     */
    void changerLangue(QString nomLangue);

    /**
     * @brief Crée le menu de langue.
     */
    void creerMenuLangue();

    QString m_langPath ; // le path pour les fichiers de langue
    QString m_langCourrant; // le nom du langage présentement chargé
};




#endif // MAINWINDOW_H
