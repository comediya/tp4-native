# TP4 native



## Générateur de codes barres Code39

Ce projet est une application Qt qui génère des codes barres basés sur le standard Code39 à partir d'une entrée textuelle.

## Prérequis
Pour compiler et exécuter ce projet, vous aurez besoin de :

    Qt 5.15 ou supérieur
    Un compilateur compatible C++11 ou supérieur

## Utilisation
L'interface utilisateur de l'application est simple :

    Saisissez le texte à coder dans le champ de texte en haut.
    Cliquez sur le bouton "Générer" pour générer le code à barres correspondant. Le code à barres apparaîtra dans la vue graphique.
    Cliquez sur le bouton "Effacer" pour effacer à la fois le champ de texte et le code à barres affiché.
    Si vous saisissez des caractères non valides (autres que A-Z, 0-9, espace, -, $, %, ., /, +), une fenêtre d'erreur apparaîtra vous invitant à corriger votre entrée.

## Notes

Ce projet utilise le format de code à barres Code39. Chaque caractère est codé par une séquence de 9 éléments, qui sont soit des barres (représentées par le chiffre 1) soit des espaces (représentés par le chiffre 0). La largeur des barres est le double de celle des espaces.

Le code de l'application est principalement contenu dans le fichier mainwindow.cpp, où se trouvent les méthodes pour générer et effacer le code à barres. Le tableau de correspondance entre les caractères et leurs représentations en Code39 est également défini dans ce fichier.
